//Vystupni piny
const int pinsOut[] = {10, 9, 8, 14, 13, 12, 11, 15};
//Vstupni pin
const int pinIn = 7;
//Doba zpozdeni
const int delayMs = 200;
//Aktuali pozice
int position;

void setup() {	
	//Nastaveni ovladaciho vstupniho pinu
	pinMode(pinIn, INPUT);

	for (int i = 0; i < (sizeof(pinsOut) / 2); i++) {
		//Nastaveni vystupnich pinu
		pinMode(pinsOut[i], OUTPUT);
		//Vypnuti vystupniho pinu
		digitalWrite(pinsOut[i], LOW);
	}

	//Nastaveni pozice na 0
	position = 0;
}

void loop() {
	//Posun vzhuru
	if(digitalRead(pinIn) == LOW) {
		position = (position < ((sizeof(pinsOut) / 2)) - 1) ? position + 1 : 0;	
	}

	//Posun dolu
	else {
		position = (position > 0) ? position - 1 : ((sizeof(pinsOut) / 2) - 1);
	}

	blink();
}

/**
	Zapne a nasledne vypne aktualni pin
*/

void blink() {
	digitalWrite(pinsOut[position], HIGH);
	delay(delayMs);
	digitalWrite(pinsOut[position], LOW);
}