//Vystupni piny
const int pinsOut[] = {10, 9, 8, 14, 13, 12, 11, 15};
//Vstupni pin
const int pinsIn[] = {16, 17};
//Doba zpozdeni
const int delaysMs[] = {50, 100, 500, 1000};
//Aktuali pozice
int position;

void blink(int);

void setup() {	
	//Nastaveni vstupnich pinu
	for (int i = 0; i < (sizeof(pinsIn) / 2); i++) {
		//Nastaveni vstupniho pinu
		pinMode(pinsIn[i], INPUT);
	}

	for (int i = 0; i < (sizeof(pinsOut) / 2); i++) {
		//Nastaveni vystupnich pinu
		pinMode(pinsOut[i], OUTPUT);
		//Vypnuti vystupniho pinu
		digitalWrite(pinsOut[i], LOW);
	}

	//Nastaveni pozice na 0
	position = 0;
}

void loop() {
	position = (position > 0) ? position - 1 : ((sizeof(pinsOut) / 2) - 1);
	int currentDelay = (digitalRead(pinsIn[0]) * 2) + (digitalRead(pinsIn[1]) * 1);
	blink(currentDelay);
}

/**
	Zapne a nasledne vypne aktualni pin
*/

void blink(int delayMs) {
	digitalWrite(pinsOut[position], HIGH);
	delay(delaysMs[delayMs]);
	digitalWrite(pinsOut[position], LOW);
}