#include <Stepper.h>
// pocet kroku na otacku dle vaseho krokoveho motoru
const int steps = 100;
const int speed = 30;
int input;

// inicializace krokoveho motoru myStepper
Stepper myStepper(steps, 16,17,2,3);

void setup() {
	Serial.begin(9600);
	myStepper.setSpeed(speed);
	Serial.println("Zadejte pocet kroku:");
}

void loop() {
	if(Serial.available()) {
		input = Serial.readString().toInt();
		myStepper.step(input);
		Serial.println("Zadejte pocet kroku:");
	}

}

