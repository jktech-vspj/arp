int input;
double oldSpeed;
int startTime;

void speedUp(int);

void setup() {
	oldSpeed = 0;
	//Output pro rychlost motoru
	pinMode(3, OUTPUT);
	//Output pro smer otaceni motoru
	pinMode(10, OUTPUT);

	Serial.begin(9600);
	Serial.println("Zadete rychlost: ");
}

void loop() {
	if(Serial.available()) {
		input = Serial.readString().toInt();

		if(input >= -100 && input <= 100) {
			speedUp(input);
		}

		else {
			Serial.println("Zadana hodnota je mimo rozsah");
		}

		Serial.println("Zadete rychlost: ");
	}
}

void speedUp(int speed) {
	//Vypocet realne rychlosti
	double realSpeed = (double)(255 / 100) * speed;
	startTime = millis();

	//Pokud se zmeni smer otaceni
	if((oldSpeed < 0 && realSpeed > 0) || (oldSpeed > 0 && realSpeed < 0)) {
		int stepsCount = abs(oldSpeed) + abs(realSpeed);

		for(int i = abs(oldSpeed); i >= 0; i--) {
			analogWrite(3, i);
			delay(2000 / stepsCount);
		}

		//Zmena smeru		
		digitalWrite(10, (speed >= 0) ? LOW : HIGH);

		for(int i = 0; i <= abs(realSpeed); i++) {
			analogWrite(3, i);
			delay(2000 / stepsCount);
		}
	}

	else {
		//Nastaveni smeru otaceni
		if (speed >= 0)	{			
			digitalWrite(10, LOW);					

		}

		else {
			digitalWrite(10, HIGH);						
		}

		if(oldSpeed < abs(realSpeed)) {
			for(int i = oldSpeed; i < abs(realSpeed); i++) {
				//Nastaveni rychlosti motoru
				analogWrite(3, abs(i));
				//Vypocet delay podle rozdilu rychlosti
				delay(2000 / abs(oldSpeed - realSpeed));
			}
		}

		else {
			for(int i = oldSpeed; i > abs(realSpeed); i--) {
				//Nastaveni rychlosti motoru
				analogWrite(3, abs(i));
				delay(2000 / abs(oldSpeed - realSpeed));
			}
		}
	}

	oldSpeed = realSpeed;
}

