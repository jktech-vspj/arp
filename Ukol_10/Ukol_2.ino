const int maxValue = 10000;
const int minValue = 2;

int startTime = 0;
long inputNum = 0;

#include "Native_LiquidCrystal.h"
Native_LiquidCrystal lcd(7, 6, 2, 3, 4, 5);

//Functions
void startTimer();
int endTimer();
bool isPrime(int);
void printToOutput(String);

void setup() {
	lcd.begin(16, 2);
	Serial.begin(9600);
	printToOutput("Zadejte cislo: ");
}

void loop() {
	if(Serial.available()) {
		inputNum = Serial.readString().toInt();
		if(inputNum >= minValue && inputNum <= maxValue) {
			startTimer();
			const int prime = isPrime(inputNum);
			int timeCost = endTimer();

			String output = String(inputNum);
			if(prime) {
				output += " je prvocislo \nVypocet trval ";
				output += timeCost;
				output += "ms";
			}

			else {
				output += " neni prvocislo \nVypocet trval ";
				output += timeCost;
				output += "mics";
			}
			printToOutput(output);
		}

		else {
			Serial.println("Zadane cislo je mimo interval: ");
		}

		Serial.println("Zadejte cislo: ");
	}

}

void startTimer() {
	startTime = micros();
}

int endTimer() {
	return (micros() - startTime);
}

bool isPrime(int number) {
	if (number < 2) {
		return false;
	}

	if (number % 2 == 0) {
		return (number == 2);
	}

	int root = (int)sqrt((double)number);
	for (int i = 3; i <= root; i += 2) {
		if (number % i == 0) {
			return false;
		}
	}
	return true;
}

void printToOutput(String text) {
	Serial.println(text);

	lcd.clear();
	lcd.setCursor(0, 0);

	int row = 0;

	for(int i = 0; i < text.length(); i++) {
		if(text[i] == '\n') {
			row += 1;
			lcd.setCursor(0, row);
		}

		else {
			lcd.print(text[i]);
		}
	}
}