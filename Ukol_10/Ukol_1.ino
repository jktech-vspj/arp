int num_a;
int num_b;
int num_c;

#include "Native_LiquidCrystal.h"
Native_LiquidCrystal lcd(7, 6, 2, 3, 4, 5);

void printToOutput(String);
String quadraticFormula(int, int, int);

void setup() {
	num_a = NULL;
	num_b = NULL;
	num_c = NULL;

	lcd.begin(16, 2);
	Serial.begin(9600);
}

void loop() {	
	readValues();
}

void readValues() {
	Serial.println("Zadejte hodnotu A: ");
	while(Serial.available()) {
		Serial.read();
	}

	while(Serial.available() == 0){	}
	num_a = Serial.parseInt();

	Serial.println("Zadejte hodnotu B: ");
	while(Serial.available()) {
		Serial.read();
	}
	while(Serial.available() == 0){	}
	num_b = Serial.parseInt();

	Serial.println("Zadejte hodnotu C: ");
	while(Serial.available()) {
		Serial.read();
	}
	while(Serial.available() == 0){	}
	num_c = Serial.parseInt();

	String output = quadraticFormula(num_a, num_b, num_c);
	printToOutput(output);
}

String quadraticFormula(int a, int b, int c) {
	String output = "";

	if(a == 0) {
		if(b != 0) {
			output = "Linearni rovnice\n";
			output += "x = ";
			output += String((((double)c * (-1)) / b), 2);
			return output;		
		}
		
		else {
			output = "Nejedna se o rovnici";
			return output;
		}
	}

	int d = pow(b, 2) - (4 * a * c);
	if(d < 0) {
		output = "Rovnice nema reseni\n";
		return output;
	}

	else {
		output += a;
		output += "x^2 + ";
		output += b;
		output += "x + ";
		output += c;
		output += " = 0\n";

		if (d == 0){
			output += "x = ";
			output += String(((-b + sqrt(d))/(2*a)), 2);
			return output;
		}

		else {
			output += "x1 = ";
			output += String(((-b + sqrt(d))/(2*a)), 2);
			output += "\nx2 = ";
			output += String(((-b - sqrt(d))/(2*a)), 2);
			return output;
		}
	}

	return output;
}

void printToOutput(String text) {
	Serial.println(text);

	lcd.clear();
	lcd.setCursor(0, 0);

	int row = 0;

	for(int i = 0; i < text.length(); i++) {
		if(text[i] == '\n') {
			row += 1;
			lcd.setCursor(0, row);
		}

		else {
			lcd.print(text[i]);
		}
	}
}