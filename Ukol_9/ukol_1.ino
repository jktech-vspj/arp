//Tento ukol jsem se snazil udelat tak, aby se prizpusoboval zadanym hodnotam nize, takze pokud se max hodnota zvedne z 199 na 1099 tak cisla budou stale spravne formatovana

//Urcuje min a max hodnotu matice
const int minSize = 3;
const int maxSize = 10;

//Urcuje minimalni a maximalni hodnotu generovanych cisel 
const int minValue = 0;
const int maxValue = 199;

int decimalCount = 0;
int input;

String formatNumber(int);
int getDecimalCount(int);
void printStartMessage();

void setup() {
	//Vypocita pocet nul z maximalni hodnoty
	decimalCount = getDecimalCount(maxValue);
	//Inicializace seriove linky
	Serial.begin(9600);
	//Vypis textu ohledne vstupu
	printStartMessage();
}

void loop() {
	//Pokud obdrzi data na seriove lince
	if(Serial.available()) {
		//Ulozi vstup do promenne
		input = Serial.readString().toInt();
		//Kontrola rozsahu zadaneho cisla
		if((input >= minSize) && (input <= maxSize)) {
			//Promenne pro vyhodnoceni minima a maxima
			int min = maxValue, max = minValue;
			
			//Vypsani samotne matice
			for(int x = 0; x < input; x++) {
				//Vypis radku matice
				for(int y = 0; y < input; y++) {
					int random = random(minValue, maxValue);

					//Overeni minima a maxima
					if(random > max) {
						max = random;
					}

					if(random < min) {
						min = random;
					}
					
					//Vypis samotneho cisla do matice
					Serial.print(formatNumber(random));
					Serial.print(" ");
				}

				Serial.print("\n");
			}
			
			//Vypis minima a maxima
			Serial.print("\n");
			Serial.print("Min: ");
			Serial.println(min);

			Serial.print("Max: ");
			Serial.println(max);
		}

		else {
			//Vypis oznameni o tom ze cislo je mimo rozsah
			Serial.print("Zadane cislo je mimo rozsah [");
			Serial.print(minSize);
			Serial.print(";");
			Serial.print(maxSize);
			Serial.println("]");
		}

		printStartMessage();
	}
}

//Zformatuje cislo podle delky
String formatNumber(int number) {
	int currentNumberDecimals = getDecimalCount(number);
	String output = "";

	for(int i = 0; i < (decimalCount - currentNumberDecimals); i++) {
		output += " ";
	}

	output += number;
	return output;
}


//Zjisti pocet cislic v zadanem cisle
int getDecimalCount(int number) {
	int count = 0;
	for(int i = number; i >= 10; i = i / 10) {
		count++;
	}

	return count;
}

//Vyzve uzivatele k zadani hodnoty
void printStartMessage() {
	Serial.print("Zadejte velikost matice ");
	Serial.print("[");
	Serial.print(minSize);
	Serial.print(";");
	Serial.print(maxSize);
	Serial.print("]: \n");
}