const int maxValue = 9999;
unsigned int number;

bool isPrime(int);
String getFactors(int);

void setup() {
	Serial.begin(9600);
	Serial.println("Zadejte cislo: ");
}

void loop() {
	//Pokud na seriovou linku prijdou data
	if(Serial.available()) {
		//Ulozi vstup do promenne
		number = Serial.readString().toInt();
		
		//Overi rozsah zadanych cisel
		if(number <= maxValue && number >= 1) {
			//Vypise vystup na seriovou linku
			String output = getFactors(number);
			Serial.println(output);
		} 

		else {
			Serial.println("Neplatny vstup");
		}
		
		Serial.println("Zadejte cislo: ");
	}
}

//Vrati rozklad nebo informaci ze se jedna o prvocislo
String getFactors(int n) {
	String output = "";
	//Vypis informace ze se jedna o prvocislo
	if(isPrime(n)) {
		output += n;
		output += " je prvocislo";
		return output;
	}

	//Vypocet rozkladu
	output += n;
	output += " = ";

	for(int i = 2; i < n; i++) {
		while(n % i == 0) {
			output += i;
			output += " * ";

			n = n / i;
		}
	}

	if(n > 1) {
		output += n;
	}

	else {
		//Z vystupu odebere "* "
		output.remove(output.length() - 1);
		output.remove(output.length() - 1);
	}

	return output;
}

//Zjisti jestli je zadane cislo prvocislo
bool isPrime(int num) {
	for(int j = 2; j < num; j++) {
		while(num % j == 0) {
			return false;
		}
	}
	return true;
}
