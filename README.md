# Architektura počítačů

### Rozdělení cvičení

- 1-5 - Logické obvody
- 7 - Test na Logické obvody
- 8-11 - Arduino
- 12 - Test na Arduino

### Popis
Vypracovaná cvičení do předmětu *Architektura počítačů* na VSPJ. U *UnoArduSim* bylo potřeba používat určitou verzi. Deeds i UnoArduSim bylo možné na linuxu spustit pomocí wine. Všechna cvičení byla schválena, ale **ne všechna na maximální počet bodů**.

### Požadavky

- [DEEDS](https://www.digitalelectronicsdeeds.com/downloads.html) (*Lze použít wine*)
- [UnoArduSim](https://sites.google.com/site/unoardusim/simulator-download) (*Lze použít wine*)